#include <iostream>
#include <cmath>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <armadillo>
#include <time.h>
using namespace arma;
using namespace std;

double offdiag(mat & A, int & p, int & q, int n)
{
  //Finding the maximum matrix elements
	double max;
	for(int i = 0; i < n; i++)
	{
		for(int j = i+1; j < n; j++)
		{
			double aij = fabs(A(i,j));
			if (aij > max)

			{
				max = aij; p = i; q = j;
			}
		}
	}
	return max;
}

void rotate(mat & A, int k, int l, int n)
{
  //Finding values for cos and sin
	double s, c;

	if(A(k,l) != 0.0)
	  {
		double t, tau;
		tau = (A(l,l) - A(k,k))/(2*A(k,l));
		if(tau >= 0)
		{
			t = 1.0/(tau + sqrt(1. + tau*tau));
		}
		else
		{
			t = -1.0/(-tau + sqrt(1. + tau*tau));
		}
		c = 1.0/sqrt(1 + t*t);
		s = t*c;
	}
	else
	{
		c = 1.0;
		s = 0.0;
	}

	//Changing matrix elements
	double a_kk, a_ll, a_ik, a_il, r_ik, r_il;
	a_kk = A(k,k);
	a_ll = A(l,l);
	A(k,k) = c*c*a_kk - 2.0*c*s*A(k,l) + s*s*a_ll;
	A(l,l) = s*s*a_kk + 2.0*c*s*A(k,l) + c*c*a_ll;
	A(k,l) = 0.0;
	A(l,k) = 0.0;
	for(int i = 0; i < n; i++)
	{
		if(i != k && i != l)
		{
			a_ik = A(i,k);
			a_il = A(i,l);
			A(i,k) = c*a_ik - s*a_il;
			A(k,i) = A(i,k);
			A(i,l) = c*a_il + s*a_ik;
			A(l,i) = A(i,l);
		}
	}
}

int main(int argc, char* argv[])
{
  //declaring variables and setting up matrix A
        string arg = "no interaction";
	if(argc > 2)
	  {
	    arg = argv[2];
	  } 
	int i, n = 100;
	double rho = 0.0, rho_max = 5.0, h = rho_max/n, V, omega_r = atof(argv[1]);
	mat A = zeros(n,n);
	vec a(n-1);
	A.diag(1) = a.fill(-1./(h*h));
	A.diag(-1) = a;
	for(i=0; i<n; i++)
	{
		rho = (i+1)*h;
		if(arg == "interaction")
		{
		  V = omega_r*omega_r*rho*rho + 1.0/rho;
		}
		else
		{ 
		  V = omega_r*omega_r*rho*rho;
	        }
		A(i,i) = 2./(h*h) + V;
	}

	//Finding eigenvalues and eigenvectors using armadillo
	vec eigval;
	mat eigvec;
	clock_t start, finish;
	start = clock();
	eig_sym(eigval, eigvec, A);
	finish = clock();
	cout << endl << "Time usage for finding eigenvalues and eigenvectors using armadillo: " << double (finish - start)/CLOCKS_PER_SEC << "s" << endl;



	//Writing eigenvectors to file for plotting
	ostringstream omega_r_string;
	omega_r_string << omega_r;
	string filename;
	if(arg == "interaction")
	  {
	    filename =  "eigenvectors_inter_omega_r=" + omega_r_string.str() + ".txt";
	  }
	else 
	  {
	    filename = "eigenvectors_omega_r=" + omega_r_string.str() + ".txt";
	  }
	const char *filename2 = filename.c_str();

	ofstream myfile;
	myfile.open(filename2);
	for(i = 0; i < n; i++){
	  myfile << setprecision(4) << eigvec(i,0) << " " << eigvec(i,1) << " " << eigvec(i,2) << endl;
	}
	myfile.close();


	int p,q;
	double eps = 1.0E-8;
	int iterations = 0;
	int maxiter = n*n*n;
	double maxnondiag = offdiag(A, p, q, n);
	start = clock();
	while(maxnondiag > eps && iterations <= maxiter)
	{
		maxnondiag = offdiag(A, p, q, n);
		rotate(A, p, q, n);
		iterations++;
	}
	finish = clock();
	cout << "Time usage for Jacobi's method: " << double (finish - start)/CLOCKS_PER_SEC << "s" << endl << endl;

	cout << "Number of iterations: " << iterations << endl << endl;

	vec temp(n);
	for(i=0; i<n; i++){
		temp(i) = A(i,i);
	}
	vec eigenvalues = sort(temp);

	//Printing the lowest eigenvalues to screen
	cout << "Lowest eigenvalues using Jacobi's method:" << endl;
	for(i=0; i<4; i++){
		cout << eigenvalues(i) << " ";
	}

	cout << endl << endl;
	cout << "Lowest eigenvalues using armadillo:" << endl;
	for(i=0; i<4; i++){
		cout << eigval(i) << " ";
	}
	cout << endl << endl;
}
