\documentclass[11pt,a4wide]{article}
\usepackage{verbatim}
\usepackage{listings}
\usepackage{graphicx}
\usepackage{a4wide}
\usepackage{color}
\usepackage{amsmath}
%\usepackage{amssymb}
%\usepackage[dvips]{epsfig}
%\usepackage[T1]{fontenc}
%\usepackage{cite} % [2,3,4] --> [2--4]
%\usepackage{shadow}
\usepackage{hyperref}

%\setcounter{tocdepth}{2}

\lstset{language=c++}
%\lstset{alsolanguage=[90]Fortran}
\lstset{basicstyle=\small}
\lstset{backgroundcolor=\color{white}}
\lstset{frame=single}
\lstset{stringstyle=\ttfamily}
\lstset{keywordstyle=\color{red}\bfseries}
\lstset{commentstyle=\itshape\color{blue}}
\lstset{showspaces=false}
\lstset{showstringspaces=false}
\lstset{showtabs=false}
\lstset{breaklines}
\begin{document}

\title{FYS - 4150}
\author{Ida Pauline Buran}
\maketitle

\section*{Project 2}
In this project we are going to solve the Schr\"odinger equation for two electrons in a harmonic oscillator well, with and without a repulsive Coulomb interaction. We are going to do this by reformulating the equation in a discretized form as an eigenvalue equation, which we will solve by writing a code that implements Jacobi's method. 

The source code of the program can be found at https://bitbucket.org/idapb/project-2/\\\\

We are first going to look at the radial part of Schr\"odinger's equation for one electron which reads
\[
  -\frac{\hbar^2}{2 m} \left ( \frac{1}{r^2} \frac{d}{dr} r^2
  \frac{d}{dr} - \frac{l (l + 1)}{r^2} \right )R(r) 
     + V(r) R(r) = E R(r),
\]
where the harmonic oscillator potential is $V(r)=(1/2)kr^2$.\\

We introduce dimensionless variables and rewrite the equation as
\[
  -\frac{d^2}{d\rho^2} u(\rho) + \rho^2u(\rho)  = \lambda u(\rho)
\]
where $R(r)=(1/r)u(r)$, $l=0$, $\rho=(1/\alpha)r$, $\alpha=(\hbar/mk)^{1/4}$ and $\lambda=(2m\alpha^2/\hbar^2)E$. \\

Using the standard expression for the second derivative we can write this as
\[
-\frac{u(\rho_i+h) -2u(\rho_i) +u(\rho_i-h)}{h^2}+\rho_i^2u(\rho_i)  = \lambda u(\rho_i),
\]
where the step length, $h$, is defined asf $(\rho_{max}-\rho_{min})/n$ for a given number of steps $n$. This can be written more compactly as
\[
d_iu_i+e_{i-1}u_{i-1}+e_{i+1}u_{i+1}  = \lambda u_i,
\]
where $d_i=(2/h^2)+V_i$, $V_i=\rho^2$ and $e_i=-1/h^2$.\\

We can write this as a matrix eigenvalue problem
\begin{equation}
    \left( \begin{array}{ccccccc} d_1 & e_1 & 0   & 0    & \dots  &0     & 0 \\
                                e_1 & d_2 & e_2 & 0    & \dots  &0     &0 \\
                                0   & e_2 & d_3 & e_3  &0       &\dots & 0\\
                                \dots  & \dots & \dots & \dots  &\dots      &\dots & \dots\\
                                0   & \dots & \dots & \dots  &\dots       &d_{n_{\mathrm{step}}-2} & e_{n_{\mathrm{step}}-1}\\
                                0   & \dots & \dots & \dots  &\dots       &e_{n_{\mathrm{step}}-1} & d_{n_{\mathrm{step}}-1}

             \end{array} \right)      \left( \begin{array}{c} u_{1} \\
                                                              u_{2} \\
                                                              \dots\\ \dots\\ \dots\\
                                                              u_{n_{\mathrm{step}}-1}
             \end{array} \right)=\lambda \left( \begin{array}{c} u_{1} \\
                                                              u_{2} \\
                                                              \dots\\ \dots\\ \dots\\
                                                              u_{n_{\mathrm{step}}-1}
             \end{array} \right) 
    \label{eq:matrix}
\end{equation} 
which can be solved using Jacobi's rotation algorithm.

\subsection*{(a)}
Our first task was to write a function implementing Jacobi's rotation algorithm to solve equation \ref{eq:matrix}. The method uses a transformation matrix, $\mathbf{S}$, to perform a rotation around an angle $\theta$ in Euclidian space. The diagonal elements of $\mathbf{S}$ are 1, and the non-diagonal elements that differ from zero are $s_{kk} = s_{ll} = \cos\theta$ and $s_{kl} = -s_{lk} = -\sin\theta$. The algorithm computes $\mathbf{S}^T\mathbf{A}\mathbf{S}$ and performs a number of iterations until the sum over the non-diagonal matrix elements of a matrix $\mathbf{A}$ are smaller than a prefixed test.\\
We defined the quantities $\tan\theta=t=s/c$ and $\cot2\theta=\tau=(a_{ll}-a_{kk})/2a_{kl}$. From this we found 
$$
t=-\tau\pm\sqrt{1+\tau^2}, \:\: c=\frac{1}{\sqrt{1+t^2}}, \:\: \text{and}\:\: s=tc.
$$
We should choose $t$ to be the smaller of the roots because this makes sure that the norm of the off-diagonal matrix elements are systematically reduced so that the matrix moves closer to diagonal form for each transformation.

\subsection*{(b)}
In order to get the lowest three eigenvalues with four leading digits we need 280 points $n$, and $\rho_{max}$ should be equal to 5. The number of similarity transformations needed before all non-diagonal matrix elements are essentially zero is shown in table \ref{tab:1} for different values of $n$. The number of transformations needed seems to be proportional to $n^2$.\\
We also found the eigenvalues of the matrix using Armadillo in order to compare the methods. We got the same results, but Jacobi's method took a lot longer to compute than the Armadillo function. The time usage for the two algorithms for different $n$ is shown in table \ref{tab:1}.


\begin{table}[h]
  \centering
  \begin{tabular}{|c|c|c|c|}
    \hline
    n & Iterations & Jacobi [s] & Armadillo [s] \\ \hline
    40 & 2580 & 0.06 & 0.00 \\ \hline
    80 & 10471 & 0.79 & 0.01 \\ \hline
    120 & 23765 & 4.15 & 0.01 \\ \hline
    160 & 42576 & 13.86 & 0.02 \\ \hline
    200 & 66618 & 29.90 & 0.04 \\ \hline
    240 & 96399 & 70.79  & 0.07 \\ \hline
    280 & 131428 & 145.01 & 0.09 \\ \hline
  \end{tabular}
  \caption{Number of iterations for Jacobi's method and the time usage for Jabobi's method versus the Armadillo function for an $n\times n$ matrix.\label{tab:1}}
\end{table}

%
%\begin{table}[h]
%  \centering
%  \begin{tabular}{|c|c|c|c|c|c|c|c|c|}
%    \hline
%    n & 40 & 80 & 100 & 140 & 180 & 200 & 240 & 280 \\ \hline
%    Iterations & 2580 & 10471 & 16532 & 32479 & 54180 & 66618 & 96399 & 131428 \\ \hline
%    Jacobi [s]
%    Armadillo [s]
%  \end{tabular}
%  \caption{.\label{tab:1}}
%\end{table}
%

\subsection*{(c)}
Next we studied two electrons in a harmonic oscillator well which also interact via a repulsive Coulomb interaction. The Schr\"odinger equation for two electrons with no Coulomb interaction is
\[
\left(  -\frac{\hbar^2}{2 m} \frac{d^2}{dr_1^2} -\frac{\hbar^2}{2 m} \frac{d^2}{dr_2^2}+ \frac{1}{2}k r_1^2+ \frac{1}{2}k r_2^2\right)u(r_1,r_2)  = E^{(2)} u(r_1,r_2) .
\]
By rewriting this equation, introducing dimensionless variables and adding the repulsive Coulomb interacion term
\[
V(r_1,r_2) = \frac{\beta e^2}{|{\bf r}_1-{\bf r}_2|}=\frac{\beta e^2}{r},
\]
we arrive at the equation
\[
  -\frac{d^2}{d\rho^2} \psi(\rho) + \omega_r^2\rho^2\psi(\rho) +\frac{1}{\rho}\psi(\rho) = \lambda \psi(\rho).
\]
Here $\alpha=\hbar^2/(m\beta e^2), \lambda=(m\alpha^2/\hbar^2)E$ and $\omega_r^2=1/4(mk/\hbar^2)\alpha^4$. $\omega_r$ is a parameter which reflects the oscillator potential. We see that we can use the same code as before but we need to change the potential from $\rho^2$ to $\omega_r^2\rho^2 + 1/\rho$.\\\\
We studied the cases $\omega_r=0.01, 0.5, 1, 5$ for the ground state. The lowest eigenvalues for the different values of $\omega_r$ are shown in table \ref{tab:2}, for both the interacting and the non-interacting system. The eigenvalues are proportional to the energy of the system and the lowest eigenvalue corresponds to the ground state. We see that the interacting system has higher energy than the non-interacting case, and the energy of the system increases as the oscillator potential gets larger.

\begin{table}[h]
  \centering
  \begin{tabular}{|c|c|c|}
    \hline
    $\omega_r$ & $\lambda_0$ with interaction & $\lambda_0$ without interaction \\ \hline
    0.01 & 0.84 & 0.39 \\ \hline
    0.5 & 2.23 & 1.50 \\ \hline
    1 & 4.06 & 3.00 \\ \hline
    5 & 17.43 & 14.98 \\ \hline
  \end{tabular}
  \caption{The lowest eigenvalue $\lambda_0$ with and without a repulsive Coulomb interaction for different values of $\omega_r$.\label{tab:2}}
\end{table}



\subsection*{(d)}
In the last exercise we plotted the probability distribution (the wave function squared) for two electrons as a function of $\rho$, for different values of $\omega_r$. The plots are shown in figure \ref{fig:0}, \ref{fig:1}, \ref{fig:2} and \ref{fig:3}. We see that the wave function in the case where there is Coulomb interaction is slightly shifted from the case without interaction, but as the harmonic oscillator potential increases, the difference becomes smaller and the repulsion will become negligible for large $\omega_r$.

\begin{figure}[h]
  \centering
    \includegraphics[width=0.7\textwidth]{project2_plot_0.pdf}
  \caption{The probability distribution for two electrons in a harmonic oscillator well, with and without a repulsive Coulomb interaction. $\omega_r$ is the strength of the oscillator potential. \label{fig:0}}
\end{figure}

\begin{figure}[h]
  \centering
    \includegraphics[width=0.7\textwidth]{project2_plot_1.pdf}
  \caption{The probability distribution for the two electrons in a harmonic oscillator well, with and without a repulsive Coulomb interaction. $\omega_r$ is the strength of the oscillator potential. \label{fig:1}}
\end{figure}

\begin{figure}[h]
  \centering
    \includegraphics[width=0.7\textwidth]{project2_plot_2.pdf}
  \caption{The probability distribution for two electrons in a harmonic oscillator well, with and without a repulsive Coulomb interaction. $\omega_r$ is the strength of the oscillator potential. \label{fig:2}}
\end{figure}

\begin{figure}[h]
  \centering
    \includegraphics[width=0.7\textwidth]{project2_plot_3.pdf}
  \caption{The probability distribution for two electrons in a harmonic oscillator well, with and without a repulsive Coulomb interaction. $\omega_r$ is the strength of the oscillator potential. \label{fig:3}}
\end{figure}


\end{document}
