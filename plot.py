from numpy import *
import matplotlib.pyplot as plt
import os

omega_r = (0.01, 0.5, 1, 5)
for i in range(4):
        os.system('./project2.x %s' %omega_r[i])
        filename = 'eigenvectors_omega_r=%g.txt' %omega_r[i]
        eigvec_1, eigvec_2, eigvec_3 = loadtxt(filename, unpack=True)
        os.system('./project2.x %s interaction' %omega_r[i])
        filename2 = 'eigenvectors_inter_omega_r=%g.txt' %omega_r[i]
        eigvec_1_inter, eigvec_2_inter, eigvec_3_inter = loadtxt(filename2, unpack=True)
    
        n = len(eigvec_1)
        rho = linspace(0,5,n)
        
        plt.figure()
	plt.subplot(3,1,1)
	plt.plot(rho,eigvec_1**2, label='No interaction')
        plt.hold('on')
        plt.plot(rho,eigvec_1_inter**2, label='With interaction')
        plt.title('Probability distribution, $\omega_r=$%g' %omega_r[i])
        plt.text(4.9, amax(eigvec_1**2), "Ground state",
                 verticalalignment='top',
                 horizontalalignment='right',
                 bbox=dict(boxstyle='square', facecolor='white'))
	plt.legend(loc=4, prop={'size':12})

	plt.subplot(3,1,2)
	plt.plot(rho,eigvec_2**2, label='No interaction')
        plt.hold('on')
        plt.plot(rho,eigvec_2_inter**2, label='With interaction')
        plt.text(4.9,amax(eigvec_3**2), "First excited state",
                 verticalalignment='top',
                 horizontalalignment='right',
                 bbox=dict(boxstyle='square', facecolor='white'))
        plt.ylabel(r'$|u(\rho)|^2$')
	plt.legend(loc=4, prop={'size':12})

	plt.subplot(3,1,3)
	plt.plot(rho,eigvec_3**2, label='No interaction')
        plt.hold('on')
        plt.plot(rho,eigvec_3_inter**2, label='With interaction')
	plt.xlabel(r'$\rho$')
        plt.text(4.9,amax(eigvec_3**2), "Second excited state",
                 verticalalignment='top',
                 horizontalalignment='right',
                 bbox=dict(boxstyle='square', facecolor='white'))
	plt.legend(loc=4, prop={'size':12})
	plt.savefig('project2_plot_%g.pdf' %i, bbox_inches='tight')
plt.show()
	
